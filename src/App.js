import './App.css';

// Componentes propios
import HolaReact from './components/HolaReact'
import DataBinding from './components/DataBinding'
import Contador from './components/Contador'
import ContadorAsync from './components/ContadorAsync'
import Sumador from './components/Sumador'
import Conversor from './components/Conversor'

function App() {
  return (
    <div className="App">
      <HolaReact texto="Hola React!"/>
      <DataBinding/>
      <Contador/>
      <ContadorAsync/>
      <Sumador numeros={[0,15,20,2,6]}/>
      <Conversor/>
    </div>
  );
}

export default App;
