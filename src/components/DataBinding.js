import React, {useState} from 'react'

export default function DataBindingTest() {
  const [nombre, setNombre] = useState("")
  const handler = (event) => {setNombre(event.target.value)}

  return (
    <div>
      {/* Se puede optar por hacer un handler como una funcion separada o como una arrow function */}
      {/* Hay consideraciones para elegir una u otra pero por ahora se puede utilizar cualquiera  */}
      <h1>Hola {nombre}</h1>
      <div>
        <label>One-way binding</label>
        <input type="text" onChange={handler} />
      </div>
      <div>
        <label>Two-way binding</label>
        <input type="text" value={nombre} onChange={(event) => {setNombre(event.target.value)}} />
      </div>
    </div>
  )
}
