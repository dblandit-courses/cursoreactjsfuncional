import React, { useState } from 'react'

const timeout = (milisegundos) => 
  new Promise(resolve => setTimeout(resolve, milisegundos))

export default function Contador() {
  const [contador, setContador] = useState(0)
  const incrementar = () => {
    timeout(1000).then(() => {
      setContador(contador+1)
    })
  }
  const reset = () => {setContador(0)}

  return (
    <div>
      <h2>Contador: {contador}</h2>
      <input type="button" value="Incrementar" onClick={incrementar}/>
      <input type="button" value="Reset" onClick={reset}/>
    </div>
  )
}
