import React from 'react'

export default function HolaReact(props) {
  return (
    <div>
      <h1>{props.texto}</h1>
      <p>{new Date().toString()}</p>
    </div>
  )
}
