import React from 'react'

const sum = (...args) => args.reduce((a, b) => a + b, 0)


export default function Sumador({numeros = []}) {
  return (
    <p>
      La suma de los numeros es: {sum(...numeros)}
    </p>
  )
}
