import React, { useState } from 'react'

export default function Contador() {
  const [contador, setContador] = useState(0)
  const incrementar = () => {setContador(contador+1)}
  const reset = () => {setContador(0)}

  return (
    <div>
      <h2>Contador: {contador}</h2>
      <input type="button" value="Incrementar" onClick={incrementar}/>
      <input type="button" value="Reset" onClick={reset}/>
    </div>
  )
}
