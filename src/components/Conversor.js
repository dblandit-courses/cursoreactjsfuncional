import React, {useState} from 'react'

export default function Conversor() {
  const [grados, setGrados] = useState(32)
  return (
    <div>
      <p>Grados Fahrenheit</p>
      <input type="number" value={grados} onChange={(event) => {setGrados(event.target.value)}} />
      <p>La temperatura en Celcius es: {(5/9) * (grados - 32)}</p>
    </div>
  )
}
